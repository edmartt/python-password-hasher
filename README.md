# PASSWORD HASHER

A script that ask the user for an input and use the hash function from passlib library. The main purpose of this project is to use a more secure way of storing users data like passwords in databases.

## Install

after clonning, follow this instructions:

On Windows:

    $ python -m venv name of the virtualenv
    $ env\Scripts\activate
    $ pip install requirements.txt

On Linux:

    $ python -m venv name of the virtualenv
    $ . env\Scripts\activate
    $ pip install requirements.txt

## Running

On Windows:

    $ python main.py

On Linux:

    $ python3 main.py


Example of use:
    
     $ python(3) main.py
     
     $ Type your plaintext password (it won't be echoed):

     $ Your hashed password: $pbkdf2-sha256$29000$4pwzppRyTkmpdQ4hhJBSig$Yg/yq9VWkrN5NF6phWiN336r.GnwdoGqSkzsOjS8xmo


## Testing

Coverage:

    $ coverage run --source=hashing,passwords.py -m unittest